//require packages

const convert = require('xml-js'),
fs = require('fs'),
xml = fs.readFileSync('the prime bazar inventory xml.xml', 'utf8'),
json = convert.xml2js(xml);

parseRows();
saveXmlToJson();

function parseRows(){
    json.elements[1].elements[4].elements[1].elements.map(row => {
        if (!row || !row.elements) return;
    
        row.elements = [
            {
                "name": "id",
                "type": "element",
                "elements": [{
                    "type": "text",
                    "text": row.elements[0].elements[0].elements[0].text
                }]
            },
            {
                "name": "title",
                "type": "element",
                "elements": [{
                    "type": "text",
                    "text": row.elements[1].elements[0].elements[0].text
                }]
            },
            {
                "name": "description",
                "type": "element",
                "elements": [{
                    "type": "text",
                    "text": row.elements[2].elements[0].elements[0].text
                }]
            },
            {
                "name": "price_usd",
                "type": "element",
                "elements": [{
                    "type": "text",
                    "text": row.elements[3].elements[0].elements[0].text
                }]
            },
            {
                "name": "sale_price_usd",
                "type": "element",
                "elements": [{
                    "type": "text",
                    "text": row.elements[4].elements[0].elements[0].text
                }]
            },
            {
                "name": "link",
                "type": "element",
                "elements": [{
                    "type": "text",
                    "text": row.elements[5].elements[0].elements[0].text
                }]
            },
            {
                "name": "image_link",
                "type": "element",
                "elements": [{
                    "type": "text",
                    "text": row.elements[6].elements[0].elements[0].text
                }]
            }
        ]
    });
}

function saveXmlToJson(){
    fs.writeFileSync('output.xml', convert.json2xml(json, {
        spaces: 2
    }));
}